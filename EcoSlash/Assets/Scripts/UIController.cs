﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class UIController : MonoBehaviour {

	[Header("UI Purposes")]
	public GameObject HealthBar;

	[Header("References")]
	public GameObject Player;

	//Hidden public variables
	[HideInInspector] public float HealthPercent;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
	
		HealthPercent = (float)Player.GetComponent<Life> ().Health / Player.GetComponent<Life> ().MaxHealth;
		HealthBar.GetComponent<Image> ().fillAmount = HealthPercent;
	}
}
