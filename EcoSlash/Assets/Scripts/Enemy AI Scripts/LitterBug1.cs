﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LitterBug1 : MonoBehaviour {

	public float moveSpeed;
	public bool right;

	public Transform FallChecker;
	public float DangerRadius;
	public LayerMask Ground;

	public Transform player;
	public int Distance;

	private bool touchingGround;

	private float enemyloc, playerloc;
	private GameObject waypoint;

	// Use this for initialization
	void Start () {
		waypoint = GameObject.FindGameObjectWithTag ("Player");
	}
			
	// Update is called once per frame
	void Update () {
		touchingGround = Physics2D.OverlapCircle (FallChecker.position, DangerRadius, Ground);

		//Patrol ();
		Chase();
	}

	public void Patrol(){

		if (!touchingGround) {
			right = !right;
		}

		if (right) {
			transform.localScale = new Vector3 (-5f, 5f, 5f);
			GetComponent<Rigidbody2D> ().velocity = new Vector2 (moveSpeed, GetComponent<Rigidbody2D> ().velocity.y);

		} else {
			transform.localScale = new Vector3 (5f, 5f, 5f);
			GetComponent<Rigidbody2D> ().velocity = new Vector2 (-moveSpeed, GetComponent<Rigidbody2D> ().velocity.y);
		}
	}

	public void Chase(){
//		playerloc = waypoint.transform.localPosition.x;
//		enemyloc = gameObject.transform.localScale.x;
//
//		if (playerloc >= enemyloc) {
//			gameObject.transform.localScale = new Vector2 (5, 0);
//		} else {
//			gameObject.transform.localScale = new Vector2 (-5, 0);
//		}
//

		if (Vector2.Distance (player.position, this.transform.position) < Distance) {
			transform.position = Vector2.MoveTowards (transform.position, player.position, moveSpeed * Time.deltaTime );
		}
	}
}
