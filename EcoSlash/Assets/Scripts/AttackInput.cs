﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttackInput : MonoBehaviour {

	public KeyCode AttackButton;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

		if (Input.GetKey (AttackButton)) {
			GetComponent<Animator> ().Play ("MeleeAttack");
		}
	}
}
