﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BreakableObjects : MonoBehaviour {

	public float TimeBeforeBreak;

	private float timerStart;
	private bool startTimer;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if (startTimer) {
			timerStart += Time.deltaTime;
		}
	}

	void OnCollisionEnter2D(Collision2D other){
		//Layer 10 is Player
		if (other.gameObject.layer == 10) {
			startTimer = true;
			if (timerStart <= TimeBeforeBreak) {
				//PlayAnimation
				Destroy (GetComponent<BoxCollider2D> ());
				GetComponent<Rigidbody2D> ().constraints = RigidbodyConstraints2D.None;
			}
		}
	}
}
