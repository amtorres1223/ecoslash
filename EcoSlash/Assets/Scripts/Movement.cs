﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movement : MonoBehaviour {

	public float MoveSpeed;
	public float JumpHeight;

	public Transform GroundCheck;
	public float GroundCheckRadius;
	public LayerMask Ground;

	public KeyCode MoveRight;
	public KeyCode MoveLeft;
	public KeyCode Jump;

	private bool doubleJump;
	private bool isGrounded;
	private float stopMove;

	private int rightDashCount; //Counter for dashes
	private int leftDashCount; 

	private float dashTimer;

	// Use this for initialization
	void Start () {
		
	}

	// Update is called once per frame
	void Update () {

		isGrounded = Physics2D.OverlapCircle (GroundCheck.position, GroundCheckRadius, Ground);
		//Made to stop sliding
		stopMove = 0f;
		DashCounter ();
		KeyInputs ();

		Debug.Log (dashTimer);

	}
		
	public void KeyInputs(){

		//Jump Mechanic
		if (isGrounded) {
			doubleJump = true;
		}

		if (Input.GetKeyDown (Jump) && isGrounded) {
			GetComponent<Rigidbody2D> ().velocity = new Vector2 (GetComponent<Rigidbody2D>().velocity.x, JumpHeight);
		} 

		if (Input.GetKeyDown (Jump) && !isGrounded && doubleJump) {
			GetComponent<Rigidbody2D> ().velocity = new Vector2 (GetComponent<Rigidbody2D> ().velocity.x, JumpHeight);
			doubleJump = false;
		}

		if (Input.GetKey (MoveLeft)) {
			transform.localScale = new Vector3 (5, 5, 5);
			stopMove = -MoveSpeed;
			leftDashCount += 1;
			Debug.Log("Walk");
		} 

		//Dashing Mechanic
		if (Input.GetKey (MoveLeft) && (leftDashCount == 1)) {
			//Dash
			Debug.Log("DASHING");
		}

		if (Input.GetKey (MoveRight)) {
			transform.localScale = new Vector3 (-5, 5, 5);
			stopMove = MoveSpeed;
		}
		//moves only when A or D is press
		GetComponent<Rigidbody2D> ().velocity = new Vector2 (stopMove, GetComponent<Rigidbody2D>().velocity.y);

	}

	public void DashCounter(){
		if ((leftDashCount == 1) && (dashTimer <= 1 )){
			dashTimer += Time.deltaTime;
		}

		if ((leftDashCount == 1) && (dashTimer >= 1)) {
			leftDashCount = 0;
			dashTimer = 0;
		}

		if ((leftDashCount == 2) && (dashTimer >= 0.5f)) {
			leftDashCount = 0;
		}
	}
}
