﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Life : MonoBehaviour {

	public int Health;
	public int MaxHealth;

	public float DeathX;
	public float DeathY;

	private int dieSide = 1;

	// Use this for initialization
	void Start () {
		Health = MaxHealth;
	}
	
	// Update is called once per frame
	void Update () {

		if (Health <= 0) {
			Die();
		}
	}

	public void TakeDamage(int dmg){
		Health -= dmg;
		PlayerDamaged
	}

	public void Die(){
		//play die animation
		//after secs die and delete
		Destroy (this);
	}

	public void PlayerDamaged(){
		if (transform.localScale.y == 1) {
			dieSide *= 1;
		}	

		if (GameObject.layer == 10) {
			GetComponent<Rigidbody2D> ().AddForce (new Vector2 (DeathX * dieSide, DeathY), ForceMode2D.Impulse);
		}
	}
		
}
