﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour {

	public Transform Player;
	public float Speed;
	public float DistanceY;
	public float DistanceX;

	void LateUpdate(){
		transform.position = Vector3.Lerp(transform.position, Player.position + new Vector3 (DistanceX,DistanceY), Speed);

	}
}
