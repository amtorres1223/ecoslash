﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DamageGiver : MonoBehaviour {

	private Life life;
	public int Damage; //Amount of damage being given to the other enemy

	// Use this for initialization
	void Start () {
		life = GetComponent<Life> ();
	}
	
	// Update is called once per frame
	void Update () {

	}

	void OnTriggerEnter2D(Collider2D other){
		if (other.gameObject.layer == 9) {
			other.GetComponent<Life>().TakeDamage (Damage);
			Debug.Log ("Igavedamage");
//			GiveDamage ();
		}
	}

//	public void GiveDamage(){
//		life.TakeDamage (Damage);
//	}
}
